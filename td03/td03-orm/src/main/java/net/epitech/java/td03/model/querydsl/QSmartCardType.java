package net.epitech.java.td03.model.querydsl;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;
import com.mysema.query.types.PathMetadata;

import javax.annotation.Generated;

import net.epitech.java.td03.model.SmartCard;
import net.epitech.java.td03.model.SmartCardType;

import com.mysema.query.types.Path;
import com.mysema.query.sql.ColumnMetadata;


/**
 * QSmartCardType is a Querydsl query type for SmartCardType
 */
@Generated("com.mysema.query.sql.codegen.MetaDataSerializer")
public class QSmartCardType extends com.mysema.query.sql.RelationalPathBase<SmartCardType> {

    private static final long serialVersionUID = 1456315956;

    public static final QSmartCardType SmartCardType = new QSmartCardType("SmartCardType");

    public final NumberPath<Integer> id = createNumber("id", Integer.class);

    public final StringPath typeName = createString("typeName");

    public final com.mysema.query.sql.PrimaryKey<SmartCardType> primary = createPrimaryKey(id);

    public final com.mysema.query.sql.ForeignKey<SmartCard> _smartCard1Fk = createInvForeignKey(id, "type");

    public QSmartCardType(String variable) {
        super(SmartCardType.class,  forVariable(variable), "null", "SmartCardType");
        addMetadata();
    }

    public QSmartCardType(Path<? extends SmartCardType> path) {
        super(path.getType(), path.getMetadata(), "null", "SmartCardType");
        addMetadata();
    }

    public QSmartCardType(PathMetadata<?> metadata) {
        super(SmartCardType.class,  metadata, "null", "SmartCardType");
        addMetadata();
    }

    public void addMetadata() {
        addMetadata(id, ColumnMetadata.named("id").ofType(4).withSize(10).notNull());
        addMetadata(typeName, ColumnMetadata.named("typeName").ofType(12).withSize(25).notNull());
    }

}

